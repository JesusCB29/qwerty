import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppMyFirstComponent } from './myFirstComponent/app.myfirstcomponent';
import { OthercomponentComponent } from './othercomponent/othercomponent.component';
import { from } from 'rxjs';
import { SampleFilterComponent } from './sample-filter/sample-filter.component';
import { TwoBindingComponent } from './two-binding/two-binding.component';


@NgModule({
  declarations: [
    AppComponent,
    AppMyFirstComponent,
    OthercomponentComponent,
    SampleFilterComponent,
    TwoBindingComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
