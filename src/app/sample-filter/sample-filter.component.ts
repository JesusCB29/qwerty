import { Component, OnInit } from '@angular/core';
import { FilterService } from './filter.Service';

@Component({
  selector: 'app-sample-filter',
  templateUrl: './sample-filter.component.html',
  styleUrls: ['./sample-filter.component.css']
})
export class SampleFilterComponent implements OnInit {

  constructor() { }

  filtersList : any=[];
  ngOnInit() {
    let filterService = new FilterService();
    this.filtersList = filterService.getFilters();
  }


}
