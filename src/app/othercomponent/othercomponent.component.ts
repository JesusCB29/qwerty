import { UserService } from "./usersService";
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-othercomponent',
  templateUrl: './othercomponent.component.html',
  styleUrls: ['./othercomponent.component.css']
})
export class OthercomponentComponent implements OnInit {

  constructor() { }
  usersList: any=[];
  ngOnInit() {
    let userService = new UserService();

    this.usersList= userService.getUsuarios(); 

  }

}
